#!/usr/bin/env bash
THIS_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)
SCRIPT_NAME="colorscript"

sed -i "s|INSERT_HERE_WITH_CONFIGURE|$THIS_DIR|g" "$THIS_DIR/$SCRIPT_NAME.sh"

sudo ln -s "$THIS_DIR/$SCRIPT_NAME.sh" /usr/local/bin/$SCRIPT_NAME
